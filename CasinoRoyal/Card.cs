﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace CasinoRoyal
{
    public class Card
    {
        public const string Clubs = "Clubs"; //♣
        public const string Diamonds = "Diamonds"; //♦
        public const string Hearts = "Hearts"; //♥
        public const string Spades = "Spades"; //♠

        public static string[] Colors = {Clubs, Diamonds, Hearts, Spades};


        public const char Ace = 'A';
        public const char Jack = 'J';
        public const char King = 'K';
        public const char Queen = 'Q';

        public static char[] Faces = {Ace, Jack, King, Queen};

        public static char[] Numbers = {'2', '3', '4', '5', '6', '7', '8', '9', '0'};

        public static char[] Cards = Faces.Concat(Numbers).ToArray();
        
        

        private string _color;
        private char _face;

        
        public string Color => _color;
        public char Face => _face;
        
        public string PrettyFace => _face == '0' ? "10" : _face.ToString();

        public string PrettyPrint => $"{PrettyFace} {Color}";
        
        
        
        
        
        public Card(string color, char face)
        {
            _color = color;
            _face = face;
        }

        
        public int GetValue()
        {
            if (Numbers.Contains(Face))
            {
                int value = int.Parse(Face.ToString());
                
                return value == 0 ? 10 : value;
            }
            else
            {
                if (!IsAce())
                {
                    return 10;
                }
                else
                {
                    return 0;
                }
            }
        }

        public bool IsAce() => _face == Ace;

        public override string ToString() => $"{PrettyFace} {Color}";
    }
}