﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CasinoRoyal
{
    internal class Program
    {
        //♣
        //♦
        //♥
        //♠

        private static int _remainingUserCoins = 20;
        
        private static int _costOfBlackjack = 1;

        public static void Main(string[] args)
        {
            enterCasino();

            Console.WriteLine("Goodbye");
            Console.ReadLine();
        }


        private static void enterCasino()
        {
            bool didTheUserDoAValidChoice = false;

            while (!didTheUserDoAValidChoice)
            {
                Console.Clear();
                Console.WriteLine("enterB; exitX; slotE");

                ConsoleKey userKey = Console.ReadKey().Key;

                switch (userKey)
                {
                    case ConsoleKey.B:
                        blackjack();
                        didTheUserDoAValidChoice = true;
                        break;
                    case ConsoleKey.E:
                        Console.WriteLine("Slot");
                        didTheUserDoAValidChoice = true;
                        break;
                    case ConsoleKey.X:
                        exitCasino();
                        didTheUserDoAValidChoice = true;
                        break;
                }
            }
        }


        private static void exitCasino()
        {
            Console.Clear();
            Console.WriteLine("AUF WIEDERSEHEN.");
            Console.WriteLine($"Sie nehmen heute {_remainingUserCoins} mit nach Hause");
        }

        private static void blackjack()
        {
            if (_remainingUserCoins < _costOfBlackjack)
            {
                Console.WriteLine("Poor boy!");
                return;
            }

            _remainingUserCoins -= _costOfBlackjack;
            
            bool isTheGameDone = false;
            
            
            
            DeckOfCards deckOfCards = new DeckOfCards(6);
            
            HandCards userCards = new HandCards();
            userCards.Add(deckOfCards.getRandomCard());
            userCards.Add(deckOfCards.getRandomCard());
            

            HandCards botCards = new HandCards();
            botCards.Add(deckOfCards.getRandomCard());
            botCards.Add(deckOfCards.getRandomCard());
            
            
            while (!isTheGameDone)
            {
                Console.Clear();

                if (userCards.GetHandValue() >= 21)
                {
                    isTheGameDone = true;
                    break;
                }
                
                bool didTheUserDoAValidChoice = false;

                while (!didTheUserDoAValidChoice)
                {
                    Console.Clear();
                    Console.WriteLine($"Your cards: {userCards.PrettyPrint()}");
                    Console.WriteLine("Do you want to take a new card? (Y/N)");

                    ConsoleKey userKey = Console.ReadKey().Key;

                    switch (userKey)
                    {
                        case ConsoleKey.Y:
                            userCards.Add(deckOfCards.getRandomCard());
                            didTheUserDoAValidChoice = true;
                            break;
                        case ConsoleKey.N:
                        case ConsoleKey.Escape:
                            isTheGameDone = true;
                            didTheUserDoAValidChoice = true;
                            break;
                    }
                }

                if (botCards.GetHandValue() < 17)
                {
                    botCards.Add(deckOfCards.getRandomCard());
                }
                if (botCards.GetHandValue() == 17 && botCards.hasAce())
                {
                    //TODO: Make soft/hard user choiceable
                    botCards.Add(deckOfCards.getRandomCard());
                }
            }
        }
    }
}