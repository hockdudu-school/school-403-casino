﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CasinoRoyal
{
    public class HandCards : ICollection<Card>
    {
        private List<Card> _cardsList = new List<Card>();
        
        
        
        public int GetHandValue()
        {
            int userHandValueWithoutAces = 0;
            int numberOfAces = 0;

            foreach (Card userCard in _cardsList)
            {
                if (userCard.IsAce())
                {
                    numberOfAces++;
                }
                else
                {
                    userHandValueWithoutAces += userCard.GetValue();
                }
            }

            //It doesn't matter anymore
            if (userHandValueWithoutAces + numberOfAces > 21)
            {
                return userHandValueWithoutAces + numberOfAces;
            }

            //No aces anyway
            if (numberOfAces == 0)
            {
                return userHandValueWithoutAces;
            }

            int userhandFinalValue = userHandValueWithoutAces + numberOfAces;

            for (int i = 0; i < numberOfAces; i++)
            {
                //If ace were 11 instead of 1
                if (userhandFinalValue + 10 <= 21)
                {
                    userhandFinalValue += 10;
                }
            }

            return userhandFinalValue;
        }

        public string PrettyPrint()
        {
            return string.Join(" | ", _cardsList);
        }

        public bool hasAce()
        {
            return _cardsList.Any(card => card.IsAce());
        }
        
        
        
        public IEnumerator<Card> GetEnumerator()
        {
            return _cardsList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Card item)
        {
            _cardsList.Add(item);
        }

        public void Clear()
        {
            _cardsList.Clear();
        }

        public bool Contains(Card item)
        {
            return _cardsList.Contains(item);
        }

        public void CopyTo(Card[] array, int arrayIndex)
        {
            _cardsList.CopyTo(array, arrayIndex);
        }

        public bool Remove(Card item)
        {
            return _cardsList.Remove(item);
        }

        public int Count => _cardsList.Count;
        
        public bool IsReadOnly { get; }
    }
}