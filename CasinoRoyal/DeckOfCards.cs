﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CasinoRoyal
{
    public class DeckOfCards : IEnumerable<Card>, ICollection<Card>
    {
        private int _numberOfDecks;
        private List<Card> _deck;
        private Random _random = new Random();

        public DeckOfCards(int numberOfDecks = 1)
        {
            _numberOfDecks = numberOfDecks;
            resetCardsSet(_numberOfDecks);
        }

        public void resetCardsSet(int numberOfDecks)
        {
            List<Card> cardsOfTheDeck = new List<Card>();

            for (int i = 0; i < numberOfDecks; i++)
            {
                foreach (string color in Card.Colors)
                {
                    foreach (char card in Card.Cards)
                    {
                        cardsOfTheDeck.Add(new Card(color, card));
                    }
                }
            }
            
            _deck = cardsOfTheDeck;
        }

        public Card getRandomCard()
        {
            if (_deck.Count == 0)
            {
                throw new IndexOutOfRangeException();
            }
            
            int randomNumber = _random.Next(_deck.Count - 1);
            
            Card cardToBeReturned = _deck[randomNumber];
            _deck.RemoveAt(randomNumber);
            
            return cardToBeReturned;
        }

        public IEnumerator<Card> GetEnumerator()
        {
            return _deck.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Card item)
        {
            _deck.Add(item);
        }

        public void Clear()
        {
            _deck.Clear();
        }

        public bool Contains(Card item)
        {
            return _deck.Contains(item);
        }

        public void CopyTo(Card[] array, int arrayIndex)
        {
            _deck.CopyTo(array, arrayIndex);
        }

        public bool Remove(Card item)
        {
            return _deck.Remove(item);
        }

        public int Count { get { return _deck.Count; }}
        
        public bool IsReadOnly { get; }
    }
}